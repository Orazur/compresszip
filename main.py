#EDITED BY ALEXANDRE LORNE
# coding: utf-8
#!/usr/bin/python3.8.2#! python
from PIL import Image
import os,time
import os.path 
from sys import *
from datetime import datetime,date,timedelta
import zipfile



# VARIABLES ENV
nom_user=os.getlogin()
chemin_base=os.getcwd()
###############




        

def Find(chemin_base,_fichier,date_jour):
    for path, dirs, files in os.walk(chemin_base):
        for filename in files: 
            try:
                date_fichier=os.path.getctime(filename)
                date_fichier=time.strftime("%d",time.gmtime(date_fichier))
            except(FileNotFoundError, IOError):
                pass
            finally:
                pass

            try:
                Supprimer(_fichier)
                if date_fichier==date_jour:
                    _fichier.append(filename)
                    extension=filename.split('.')[-1]
                    for my_file in _fichier:
                        extension=my_file.split('.')[-1]
                        if(extension != 'jpg' and extension !='png'and extension !='jpeg'):
                            _fichier.remove(my_file)
                            return _fichier
            except (FileNotFoundError, IOError):
                print("le fichier est introuvable ou a ete supprimé")
            finally:
                pass

            


def Compression(_fichier):
    for fichier in _fichier:
        extension=fichier.split('.')[-1]
        chemin_image = os.path.join(os.getcwd(),fichier)
        try:
            picture=Image.open(chemin_image)
            picture.save("Compressed_"+fichier,'JPEG',optimize=True,quality=50)
        except (FileNotFoundError, IOError):
            print("erreur de droit")
        finally:
            pass
        _fichier.remove(fichier) 
        return _fichier,fichier


def Supprimer(_fichier):
    for fichier in _fichier:
        if'Compressed'==fichier[0:10]:
            _fichier.remove(fichier)
            return _fichier


if __name__ == "__main__":
    fileZip="real_picture.zip"
    _fichier=list()#LISTE DES NOMS DES FICHIERS
    chemin_base=os.getcwd()
    f=zipfile.ZipFile(fileZip,'a')
    res=True
    while res!=False:
        date_jour=(datetime.now().strftime("%d"))
        Find(chemin_base,_fichier,date_jour)
        for a in _fichier:
            f.write(a)
            os.remove(a)
        fichier=Compression(_fichier)
        time.sleep(15)
    del(_fichier)
        
       


# SI BESOIN AJOUTER UNE TACHE AVEC PLANIFICATEUR DE TACHE DE WINDOW
#Linux : crontab

